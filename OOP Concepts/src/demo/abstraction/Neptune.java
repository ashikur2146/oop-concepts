package demo.abstraction;

public class Neptune extends Planet {
	
	private final static double GRAVITY = 1.12;

	public Neptune(double mass) {
		super(mass);
	}

	@Override
	public double weight() {
		return super.getMass() * GRAVITY;
	}
}
