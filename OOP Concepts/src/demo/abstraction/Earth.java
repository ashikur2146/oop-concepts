package demo.abstraction;

public class Earth extends Planet {
	
	private final static double GRAVITY = 1.0;
	
	public Earth(double mass) {
		super(mass);
	}

	@Override
	public double weight() {
		return super.getMass() * GRAVITY;
	}
}
