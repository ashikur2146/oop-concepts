package demo.abstraction;

public class Mars extends Planet {

	private final static double GRAVITY = 0.38;

	public Mars(double mass) {
		super(mass);
	}

	@Override
	public double weight() {
		return super.getMass() * GRAVITY;
	}
}
