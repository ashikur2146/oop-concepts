package demo.abstraction;

public abstract class Planet {
	public abstract double weight();
	
	private double mass;

	public Planet(double mass) {
		super();
		this.mass = mass;
	}

	public double getMass() {
		return mass;
	}
}
