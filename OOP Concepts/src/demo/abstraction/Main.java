package demo.abstraction;

public class Main {
	public static void main(String[] args) {
		Planet earth = new Earth(10);
		Planet mars = new Mars(earth.getMass());
		Planet neptune = new Neptune(earth.getMass());
		
		System.out.println("Weight on earth is: " + earth.weight());
		System.out.println("Weight on mars is: " + mars.weight());
		System.out.println("Weight on neptune is: " + neptune.weight());
	}
}
