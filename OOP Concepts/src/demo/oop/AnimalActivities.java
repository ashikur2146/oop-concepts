package demo.oop;

public class AnimalActivities implements Animal {
	private double speed;
	
	public AnimalActivities(double speed) {
		super();
		this.speed = speed;
	}

	public String runs() {
		if (this.speed < 10)
			return "This animal is slow";
		return "This animal is fast";
	}

	public double getSpeed() {
		return speed;
	}
	
	public String animalTypeBySpeed() {
		if (this.getSpeed() < 10)
			return "This is a mammal";
		else if (this.getSpeed() >= 10 && this.getSpeed()  < 50)
			return "This is a nocturnal.";
		return "This is a bird.";
	}
}