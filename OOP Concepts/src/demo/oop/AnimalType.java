package demo.oop;

public class AnimalType {
	public String animalTypeBySpeed(AnimalActivities animal) {
		if (animal.getSpeed() < 10)
			return "This is a mammal";
		else if (animal.getSpeed() >= 10 && animal.getSpeed()  < 50)
			return "This is a nocturnal.";
		return "This is a bird.";
	}
}
