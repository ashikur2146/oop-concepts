package demo.inheritance;

public class Main {
	public static void main(String[] args) {
		Animal dog = new Dog(20);
		dog.makeSound();
		dog.move();
		
		Animal cat = new Cat(15);
		cat.makeSound();
		cat.move();
		
		Animal human = new Human(10);
		human.makeSound();
		human.move();
	}
}
