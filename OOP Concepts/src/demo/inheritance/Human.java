package demo.inheritance;

public class Human extends Animal {

	public Human(double speed) {
		super(speed);
	}

	@Override
	public void makeSound() {
		System.out.println("Human speaks.");
	}
	
	@Override
	public void move() {
		System.out.println("Human can move at an speed: " + super.getSpeed());
	}
}
