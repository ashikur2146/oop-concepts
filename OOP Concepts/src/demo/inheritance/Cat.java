package demo.inheritance;

public class Cat extends Animal {

	public Cat(double speed) {
		super(speed);
	}
	
	
	@Override
	public void makeSound() {
		System.out.println("Cat meows.");
	}
	
	@Override
	public void move() {
		System.out.println("Cat can move at an speed: " + super.getSpeed());
	}
}
