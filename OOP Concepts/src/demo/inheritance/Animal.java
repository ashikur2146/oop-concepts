package demo.inheritance;

public class Animal {
	
	private double speed;
	
	public Animal(double speed) {
		super();
		this.speed = speed;
	}

	public void makeSound() {
		System.out.println("Animal makes sound.");
	}
	
	public void move() {
		System.out.println("This animal can move at an speed: " + this.speed);
	}

	public double getSpeed() {
		return speed;
	}	
}
