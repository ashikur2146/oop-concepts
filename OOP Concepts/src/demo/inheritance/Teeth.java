package demo.inheritance;

public class Teeth {
	private double length;
	private double width;
	private double radius;
	public Teeth(double length, double width, double radius) {
		super();
		this.length = length;
		this.width = width;
		this.radius = radius;
	}
	public double getLength() {
		return length;
	}

	public double getWidth() {
		return width;
	}
	public double getRadius() {
		return radius;
	}	
}
