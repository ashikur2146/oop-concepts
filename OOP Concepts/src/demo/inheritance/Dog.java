package demo.inheritance;

public class Dog extends Animal {

	public Dog(double speed) {
		super(speed);
	}
	
	@Override
	public void makeSound() {
		System.out.println("Dog barks.");
	}
	
	@Override
	public void move() {
		System.out.println("Dog can move at an speed: " + super.getSpeed());
	}
	
	public void move(int n) {
		
	}
	
	public void move(double n) {
		
	}
}
